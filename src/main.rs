extern crate argparse;

use std::net::UdpSocket;
use std::io;
use std::io::prelude::*;
use std::process;

use argparse::{ArgumentParser, Store};

enum ExitCode {
    ArgumentError = 1
}

fn main() {

    // Store command-line arguments
    let mut udp_from_socket = "".to_string();
    let mut udp_to_address = "".to_string();

    // Parse command-line arguments
    {
        let mut ap = ArgumentParser::new();
        ap.set_description(".");
        ap.refer(&mut udp_from_socket)
            .add_option(&["-f", "--from-socket"], Store, "The UDP socket to transmit from.")
            .required();
        ap.refer(&mut udp_to_address)
            .add_option(&["-t", "--to-address"], Store, "The UDP address to transmit to. Of the form IP:PORT.")
            .required();
        ap.parse_args_or_exit();
    }

    let tx_socket_num: usize = match udp_from_socket.parse() {
        Ok(t) => t,
        Err(_) => { println!("Improper UDP reception socket specified ({})", udp_from_socket); process::exit(ExitCode::ArgumentError as i32)}
    };

    let socket = UdpSocket::bind(("localhost", tx_socket_num as u16)).unwrap();

    let stdin = io::stdin();
    for line in stdin.lock().lines() {
        socket.send_to(line.unwrap().as_bytes(), &*udp_to_address).unwrap();
    }
}
